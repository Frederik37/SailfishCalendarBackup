# SailfishCalendarBackup (FROZEN)
Create local backup of native Sailfish OS calendar database on Jolla smartphone  
Deutschsprachige Leser: Startet eine Diskussion (_Issue_).

_I'd call this state BETA, use at your own risk_

_Edit 2016-12-07:_ __FROZEN__ _Started using a new Jolla phone, as of now,_ __without__ _CalDAV or any "cloud" based calendar. Moved all entries successfully from CalDAV to Default via SQLite Manager. At the moment, I rely on regular, native Jolla calender backup which is capable of saving the entries.
Will keep this here for historical reasons, or continuation at some time in the future..._

_Edit 2018-06-28:_ Created copy at GitLab

## Background
I used to connect to a CalDAV account. As I do not have access to the CalDAV server (anymore), my only source is the Jolla device itself. Calendar is stored at the location:
/home/nemo/.local/share/system/privileged/Calendar/mkcal/db

It happened to me that this local calender was deleted, or empty, for whatever reason. Database size was 0 (zero). Jolla's native backup did not contain my CalDAV calendar. So I copy the calendar to the SD card of my device from time to time. This allows me to restore my calendar, just in case. Had to to this once or twice, successfully.

Note there is a German user community, some discussion here on GitHub, and some on together.jolla.com (tjc):

* [German community jollausers.de](https://jollausers.de/ "Visit website")
* [Discussion on GitHub](https://github.com/nemomobile/buteo-sync-plugin-caldav/issues/45 "Visit website")
* [CalDAV@tjc](https://together.jolla.com/questions/scope:all/sort:activity-desc/page:1/query:caldav/ "Visit website")

## Motivation
Steps to create a backup were cumbersome using the shell on the device, so I decided to create a script that makes backup easier.


## Tools used
As _Sailfish OS_ is somehow based on Linux, very basic Linux Bash/shell scripting knowledge is useful (_but not strictly required_) about the commands: _stat, chown, chgrp, chmod, cat, cp_

---
# 1) Create a backup
_I use_ __Linux Mint 15__ _here, which is Ubuntu, which is Debian. For Mac or Windows users, connecting may be different._

## 1.1) What it does
After successfully setting things up (see below), you will find on your SD card
* Copy of the calendar database file _db_ including timestamp, e.g.: _db_20161023_194751_ (no, there is __no__ file extension)
* Copy of the info file _db.changed_ including timestamp, e.g.: _db.changed_20161023_194751_
* Logfile: _calendarLog.txt_

Command line (Bash/shell) displays progress and finally, the log file is listed

## 1.2) What you need
Copy the files to your Jolla device's home directory using a tool of your choice (GUI, SSH, use ejected SD card as clipboard...)  
 a) sd-cardname.txt (check out _sd-cardname_sample.txt_ for an example)  
 b) cpCal.sh  
 c) fileinfo.sh  
Folder is called '_Mass Storage_' if you use a file browser; Bash says it's '_/home/nemo_'

## 1.3) Step by step
Designations:  
* Jolla = Jolla mobile device (your smartphone)
* Computer = Machine your Jolla is connected to, I assume, via USB

### 1.3.1) On Jolla
1. Make sure your Jolla device has access to WLAN or mobile data
2. Enable developer mode on Jolla (_Settings - Developer mode_)
3. Allow SSH connection and note the IP address (_I use WLAN to connect from my computer, as USB-IP did not satisfy me. Alternatively, ask your router's configuration._)
4. Create a (soft) password, e.g. '_me_'
5. Connect your Jolla device to a computer via USB
6. Download files a) to c) - see above - to your computer and pass them on to your Jolla

### 1.3.2) On your computer
Note these steps are only required __once__.  

1. Fire up a Bash/shell/terminal  
2. In Bash, connect to Jolla using SSH:  
	#> ssh nemo@IP-OF-YOUR-JOLLA _[Press Enter]_  
	Enter password _[Press Enter]_
3. You are now located in the location: '_/home/nemo_'
4. Run as __root__ / super-user: #> devel-su
5. To verify the file path, run _pwd_ (Print Working Directory): #> pwd
6. Find out about the files you recently added: #> ls -lisah
7. This should create an output similar to this:
>100418 4.0K -rwxrwxr-- 1 nemo privileged 1.4K Oct 23 18:48 cpCal.sh  
100402 4.0K -rwxrwxr-- 1 nemo privileged 1.4K Oct 23 17:35 fileinfo.sh  
100400 4.0K -rwxrwxr-- 1 nemo privileged   10 Oct 23 17:35 sd-cardname.txt

8. Make sure all files are owned by the __user__ nemo, e.g.: #> chown -v nemo <filename>
9. Make sure all files are assigned to the __group__ privileged: #> chgrp -v privileged <filename>
10. Adjust access mode of all files: #> chmod -v 774 <filename>
11. Both _.sh_ files must be __executable__. When in doubt, run: #> chmod +x <filename.sh>
12. Execute the copy command: #> ___./cpCal.sh___
13. Check shell output and SD card location

### 1.3.3) Sample output
The _cpCal.sh_ script copies two files to your SD card; location is given by _sd-cardname.txt_. After copy process has finished, _fileinfo.sh_ is called (inside of _cpCal.sh_) to display information about the calendar database __source__ file.
>[root@Sailfish nemo] #> ./cpCal.sh  
=== Kopiere Kalender: Anfang / Calendar copy: Start ===  
--- Logdatei / Logfile: /media/sdcard/9016-4EF8/calendarLog.txt ---  
Ausgeführt / Executed: 20161023_194752    '_date + time_

>--- Datei-Informationen / File info:  _/home/nemo/.local/share/system/privileged/Calendar/mkcal/db_ ---  
Ort des Einhängens / Mount point: ?  
Entstehungszeit der Datei / File created: ?  
Letzter Zugriff / Last access: 2015-11-19 07:31:33.501056424 +0100  
Letzte Modifikation / Last modification: 2016-10-22 12:21:39.300987482 +0200  
Letzte Änderung / Time of last change: 2016-10-22 12:21:39.300987482 +0200  
Gesamtgröße in Bytes / Block size: 946176  
_Im Zweifel teile durch / When in doubt divide by: 1024_

>--- Datei-Informationen / File info:  /home/nemo/.local/share/system/privileged/Calendar/mkcal/db.changed ---  
Ort des Einhängens / Mount point: ?  
Entstehungszeit der Datei / File created: ?  
Letzter Zugriff / Last access: 2015-11-20 19:04:22.431473577 +0100  
Letzte Modifikation / Last modification: 2016-10-22 12:21:39.300987482 +0200  
Letzte Änderung / Time of last change: 2016-10-22 12:21:39.300987482 +0200  
Gesamtgröße in Bytes: / Block size: 0  
_Im Zweifel teile durch / When in doubt divide by: 1024_

>=== Kopiere Kalender: Ende / Calendar copy: End ===

(Note: _db_ size (here) is 946176 Bytes, divided by 1024 this makes 924kB --> fine :-); _db.changed_ size __is__ 0 (zero). _db.changed_ is for information only, but IHMO, these two files belong together.)

### 1.3.4) Cleanup your Jolla

14. Quit superuser again: #> Exit
15. Quit shell: #> Exit
16. Shutdown _Developer mode_

__That's it!__ From now on, you should be able to run the _cpCal.sh_ script via __local__ shell on your Jolla device (developer mode required + _devel-su_, otherwise you'll get access denied messages), without connecting the Jolla device to a computer.

## 1.4) Database hands on
If you want to dive deeper into Jolla's calendar database:
* File is called _db_, __without__ file ending
* File type is _SQLite_
* You'd like to check its contents via tool of your choice, e.g. [SQLite Manager](https://addons.mozilla.org/de/firefox/addon/sqlite-manager/ "Manage any SQLite database on your computer")

---
# 2) Restore your calendar
_Be_ ___very___ _careful, as you will overwrite your calendar_

## 2.1) Summary
* On Jolla, calendar is stored at _/home/nemo/.local/share/system/privileged/Calendar/mkcal/db_  
* You will __overwrite__ the existing _db_ with a recent copy from SD
* Run the copy command _cp_ in reverse order
* Possible in _Developer mode_ on Jolla, using Sailfish OSes native shell, no (remote) computer required

## 2.2) Copy backup on SD
* Copy syntax is: _COPY source destination_, see [cp(1) - Linux man page](https://linux.die.net/man/1/cp "Copy command manpage")
* We use the copy _cp_ command in order to keep the backup (otherwise, use move _mv_): #> cp _db_20161023_194751_ db
* And, as we are clean workers, copy _db.changed_ as well:  #> cp _db.changed_20161023_194751_ db.changed
* Check success via file listing: #> _ls -lisah_
* Bash output shows __two__ files with __recent__ timestamp: _db_ and _db.changed_
* Make sure you know what you are going to do now, as the next command will overwrite the 'real' calendar Jolla uses

## 2.3) Restore
* Close down the Jolla calendar app (_if running_)
* Take a breath
* #> cp _db_ /home/nemo/.local/share/system/privileged/Calendar/mkcal/db
* #> cp _db.changed_ /home/nemo/.local/share/system/privileged/Calendar/mkcal/db.changed
* When in doubt, check user rights: are you '_nemo_', or '_root_'? #> whoami

# 3) ToDo
+ _Edit 2016-12-07: project is_ __FROZEN__, _see comment at the top; feel free to contact me for recovery_
+ Error handling in the .sh files
+ Descpription for Mac/Win users: Pull request
+ Improvement of almost everything I typed. Guess there are some English native speakers around --> Pull request ;-)
+ Script for automating the copy process to prepare restoring: cp _latest_ db _with timestamp_ from SD to _db_ on Jolla?
