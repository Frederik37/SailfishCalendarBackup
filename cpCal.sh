#!/bin/bash
# cpCal.sh
# Created:	2016-10-23 fm spheniscus@freenet.de
# Edit 01:	2016-10-26 fm Info messages echoed in English, additionally
#
# Copy Jolla calendar database on Sailfish OS device to SD-Card
# requires: fileinfo.sh, sd-cardname.txt
#
# Run as root (!) on Jolla device: devel-su
#
# Todo:	Error handler if files, or paths, do not exist, or no SD-Card found
#				Run as 'ordinary' user (e.g. nemo)?

# Calendar is stored here:
pathToCal='/home/nemo/.local/share/system/privileged/Calendar/mkcal/';

# SD-card has a name, get it from here (user may easily change this)
nameOfSD=$(cat sd-cardname.txt);

# SD-Card MUST be mounted, e.g. here:
pathToSD='/media/sdcard/'$nameOfSD;

# Simply the filename of the calendar database...
calDB=$pathToCal'db';

# ... and the .changed date
calDBch=$pathToCal'db.changed';

# Output filenames and log
outDB=$pathToSD;
	outDB+='/db_'$(date +"%Y%m%d_%H%M%S");
outDBch=$pathToSD;
	outDBch+='/db.changed_'$(date +"%Y%m%d_%H%M%S");
myLog=$pathToSD'/calendarLog.txt';


# Inform user about progress
echo '=== Kopiere Kalender: Anfang / Calendar copy: Start ===';
# Debug info, uncomment if necessary to display in Bash what happens
#echo
#echo '--- in ---';
#echo $pathToCal;
#echo $nameOfSD;
#echo $pathToSD;
#echo $calDB;
#echo $calDBch;
#echo '--- out ---';
#echo $outDB;
#echo $outDBch;
#echo $myLog;
#echo;

# Copy two files:
cp $calDB $outDB;
cp $calDBch $outDBch;

# Information/logfile, replaced each time script runs
echo '--- Logdatei / Logfile:' $myLog '---' > $myLog;

echo 'Ausgeführt / Executed: '$(date +"%Y%m%d_%H%M%S") >> $myLog;
./fileinfo.sh $calDB >> $myLog;
./fileinfo.sh $calDBch >> $myLog;
# Show logfile
cat $myLog;
echo '=== Kopiere Kalender: Ende / Calendar copy: End ===';
echo;
