#!/bin/bash
# fileinfo.sh
# Created:	2016-10-23 fm spheniscus@freenet.de
# Edit 01:	2016-10-26 fm Info messages echoed in English, additionally
#
# Get some information about file given as parameter $1 when called, e.g.
# ./fileinfo.sh <MyFileName> --> <MyFileName> goes into $1
#
# Todo:	Error handler if no file is provided $1

# See command: stat --help, e.g. at https://linux.die.net/man/1/stat
fileToCheck=$1;
fileMountedAt=$(stat -c %m $fileToCheck);
fileCreated=$(stat -c %w $fileToCheck);
lastUsed=$(stat -c %x $fileToCheck);
lastModified=$(stat -c %y $fileToCheck);
lastChanged=$(stat -c %z $fileToCheck);
fileSize=$(stat -c %s $fileToCheck);
fileName=$fileToCheck;

echo;
echo '--- Datei-Informationen / File info:			' $fileToCheck '---';
# %m is not documented in the manual?
#  My OSes help (in German) found it
echo 'Ort des Einhängens / Mount point:					'	$fileMountedAt;
# %w is not documented in the manual?
#  My OSes help (in German) found it
echo 'Entstehungszeit der Datei / File created: '	$fileCreated;
echo 'Letzter Zugriff / Last access					    ' $lastUsed;
echo 'Letzte Modifikation / Last modification:	' $lastModified;
echo 'Letzte Änderung / Time of last change:		' $lastChanged;
echo 'Gesamtgröße in Bytes / Block size:				' $fileSize;
echo 'Im Zweifel teile durch / When in doubt divide by: 1024';
echo;
